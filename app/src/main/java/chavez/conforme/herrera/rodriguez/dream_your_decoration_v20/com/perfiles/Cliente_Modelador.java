package chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.perfiles;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.Contrato;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin.MainActivity;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.R;

public class Cliente_Modelador extends AppCompatActivity {
    Button btnContratar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente__modelador);
        setUpActionBar();

        btnContratar = (Button)findViewById(R.id.btnContratarModelador);
        btnContratar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Cliente_Modelador.this, Contrato.class);
                startActivity(intent);
            }
        });
    }

    private void setUpActionBar() {

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);

        }
    }

}
