package chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.administrador;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.R;

public class ConsultarContratos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_contratos);
        setUpActionBar();
    }

    private void setUpActionBar() {
        ActionBar actionBar= getSupportActionBar();
        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);

        }
    }
}
