package chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.Modal;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.R;

public class Perfil_Modelador_NoLogin extends AppCompatActivity {
    Button buttonContrato;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil__modelador__no_login);
//Creamos el metodo para regresar una activity
        setUpActionBar();

        buttonContrato = (Button)findViewById(R.id.Contratar);

//declaramos un modal que muestre un mensaje para que el usuario se registre
        buttonContrato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openModal();
            }
        });
    }
//definimos la flecha de retorno
    private void setUpActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);

        }
    }

    private void openModal() {

        Modal exampleDialog = new Modal();
        exampleDialog.show(getSupportFragmentManager(), "example dialog");
    }
}
