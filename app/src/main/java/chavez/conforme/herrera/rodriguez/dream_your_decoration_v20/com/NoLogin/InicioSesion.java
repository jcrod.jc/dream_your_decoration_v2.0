package chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.R;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.perfiles.InicioCliente;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.perfiles.PerfilAdministrador;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.perfiles.PerfilModelador;

public class InicioSesion extends AppCompatActivity {
    Button botonIniciarSesion;
    EditText editTextUsuario, editTextClave;
    CheckBox mostrar_clave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_sesion);
        setUpActionBar();

        editTextUsuario = (EditText) findViewById(R.id.editTextusuarioC);
        editTextClave = (EditText) findViewById(R.id.editText_claveC);
        botonIniciarSesion = (Button) findViewById(R.id.buttonIS);
        mostrar_clave = (CheckBox) findViewById(R.id.mostrar_clave);


        //Aqui creamos funcion para que al seleccionar el checkBox muestre la contraseña
        mostrar_clave.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editTextClave.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    editTextClave.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        botonIniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String usuario = editTextUsuario.getText().toString();
                String clave = editTextClave.getText().toString();


                if (usuario.equals("modelador@facci.com") && clave.equals("12345")) {
                    Intent intent = new Intent(getApplicationContext(), PerfilModelador.class);
                    startActivity(intent);

                } else if (usuario.equals("administrador@facci.com") && clave.equals("12345")) {
                    Intent intent = new Intent(getApplicationContext(), PerfilAdministrador.class);
                    startActivity(intent);
                } else if (usuario.equals("cliente@facci.com") && clave.equals("12345")) {
                    Intent intent = new Intent(getApplicationContext(), InicioCliente.class);
                    startActivity(intent);


                } else if (usuario.equals("")) {

                    editTextUsuario.setError(getString(R.string.campo_obligatorio));
                    editTextUsuario.requestFocus();
                    return;


                } else if (clave.equals("")) {
                    editTextClave.setError(getString(R.string.campo_obligatorio));
                    editTextClave.requestFocus();
                    return;


                } else {
                    Toast.makeText(getApplicationContext(), "¡Usuario o Contraseña incorrectos!", Toast.LENGTH_SHORT).show();

                }


            }
        });
    }

    private void setUpActionBar() {

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);

        }
    }

}
