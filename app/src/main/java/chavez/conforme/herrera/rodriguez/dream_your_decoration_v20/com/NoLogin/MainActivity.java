package chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.AdapterDatos;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.Modeladores;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.R;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{

    //construimos una lista igual a la de adapterDato porque es la que se va a recibir como parametro a esta clase
    ArrayList<Modeladores> listaModeladores;
AdapterDatos adapter;
   RecyclerView recyclerView;
    private BottomNavigationView bottomNavigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listaModeladores = new ArrayList<>();

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);

        recyclerView = (RecyclerView) findViewById(R.id.RecivlerView);




        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        llenarPersonajes();

         adapter = new AdapterDatos(listaModeladores);
        recyclerView.setAdapter(adapter);





        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Perfil_Modelador_NoLogin.class);
                startActivity(intent);
            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                if (menuItem.getItemId() == R.id.sesionItem) {
                    Intent intent = new Intent(MainActivity.this, InicioSesion.class);
                    startActivity(intent);

                } else if (menuItem.getItemId() == R.id.registrarseItem) {
                    Intent intent = new Intent(MainActivity.this, RegistroInicio.class);
                    startActivity(intent);

                }
                return false;
            }
        });
    }

    private void llenarPersonajes() {

        listaModeladores.add(new Modeladores("Malexa Herrera","Manta - Ecuador", R.drawable.imagen_mujer,"$ 10"));
        listaModeladores.add(new Modeladores("Jose Chavez","Guayaquil - Ecuador", R.drawable.imagen_hombre,"$ 15"));
        listaModeladores.add(new Modeladores("Marjorie Conforme","Manta - Ecuador", R.drawable.imagen_mujer,"$ 12"));
        listaModeladores.add(new Modeladores("Jean Rodriguez","Quito - Ecuador", R.drawable.imagen_hombre,"$ 10"));
        listaModeladores.add(new Modeladores("Maria Choez","Cuenca - Ecuador", R.drawable.imagen_mujer,"$ 10"));
        listaModeladores.add(new Modeladores("Jayden Rodriguez","Manta - Ecuador", R.drawable.imagen_hombre,"$ 15"));
        listaModeladores.add(new Modeladores("ALisson Herrera","Guayaquil - Ecuador", R.drawable.imagen_mujer,"$ 13"));
        listaModeladores.add(new Modeladores("Stefy Calderon","Jaramijo - Ecuador", R.drawable.imagen_mujer,"$ 11"));
        listaModeladores.add(new Modeladores("Galo Quiñonez","Portoviejo - Ecuador", R.drawable.imagen_hombre,"$ 10"));
        listaModeladores.add(new Modeladores("Jose Zambrano","Manta - Ecuador", R.drawable.imagen_hombre,"$ 14"));
        listaModeladores.add(new Modeladores("Genesis Pico","Quito - Ecuador", R.drawable.imagen_mujer,"$ 12"));
    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Seguro que quiere salir de la Aplicación");
        builder.setTitle("Cerrar Aplicación");
        builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        AlertDialog dialog=builder.create();
        dialog.show();
    }
    //Creamos el buscador

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.buscador,menu);
        MenuItem menuItem=menu.findItem(R.id.buscador);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        newText = newText.toLowerCase();
        ArrayList<Modeladores> newList = new ArrayList<>();
        for (Modeladores modeladores : listaModeladores){
            String name = modeladores.getNombre().toLowerCase();
            String precio = modeladores.getPrecio().toLowerCase();
            String ciudad = modeladores.getCiudad().toLowerCase();


            if (name.contains(newText)| precio.contains(newText)|ciudad.contains(newText))
                newList.add(modeladores);


        }
            adapter.setFilter(newList);
        return true;
    }
}
