package chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin.MainActivity;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin.Perfil_Modelador_NoLogin;

public class AdapterDatos extends RecyclerView.Adapter<AdapterDatos.ViewHolderDatos> implements View.OnClickListener {
//cramos el constructor listDato y generamos el constructor
    //a este adaptador le va a llegar una lista de datos
    ArrayList<Modeladores> listaModeladores;
    private View.OnClickListener listener;

    public AdapterDatos(ArrayList<Modeladores> listaModeladores) {
        this.listaModeladores = listaModeladores;
    }
//esta parte nos va a enlazar este adaptador con el archivo creado item_recycler
    @NonNull
    @Override
    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler,null,false);

        view.setOnClickListener(this);

        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDatos holder, int position) {

        holder.nombre.setText(listaModeladores.get(position).getNombre());
        holder.ciudad.setText(listaModeladores.get(position).getCiudad());
        holder.foto.setImageResource(listaModeladores.get(position).getFoto());
        holder.precio.setText(listaModeladores.get(position).getPrecio());



    }

    @Override
    public int getItemCount() {
        return listaModeladores.size();

    }
    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }


    @Override
    public void onClick(View view) {

        if (listener!=null){
            listener.onClick(view);
        }

    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {
        //referenciamos los componentes del recurso item_recycler que es el que se va a mostrar en el RecyclerView

        TextView  nombre,ciudad,precio;
        ImageView foto;
        public ViewHolderDatos(@NonNull View itemView) {
            super(itemView);

            nombre =itemView.findViewById(R.id.idNombre);
            ciudad =itemView.findViewById(R.id.idCiudad);
            foto = itemView.findViewById(R.id.idImagen);
            precio =itemView.findViewById(R.id.idPrecio);
        }


    }

    public void setFilter(ArrayList<Modeladores> newList){
       listaModeladores=new ArrayList<>();
       listaModeladores.addAll(newList);
       notifyDataSetChanged();
    }

}
