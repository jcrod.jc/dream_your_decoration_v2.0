package chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.perfiles;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.AdapterDatos;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.Contrato;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.Modeladores;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin.InicioSesion;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin.MainActivity;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin.Perfil_Modelador_NoLogin;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin.RegistroInicio;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.R;

public class InicioCliente extends AppCompatActivity implements SearchView.OnQueryTextListener{



    ArrayList<Modeladores> listaModeladores;
    AdapterDatos adapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_cliente);
        listaModeladores = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.RecivlerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        llenarPersonajes();

         adapter = new AdapterDatos(listaModeladores);
        recyclerView.setAdapter(adapter);

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InicioCliente.this, Cliente_Modelador.class);
                startActivity(intent);
            }
        });





    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Seguro que quiere cerrar la Sesión");
        builder.setTitle("Cerrar Sesión");
        builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        AlertDialog dialog=builder.create();
        dialog.show();
    }

    private void llenarPersonajes() {

        listaModeladores.add(new Modeladores("Malexa Herrera","Manta - Ecuador", R.drawable.imagen_mujer,"$ 10"));
        listaModeladores.add(new Modeladores("Jose Chavez","Guayaquil - Ecuador", R.drawable.imagen_hombre,"$ 15"));
        listaModeladores.add(new Modeladores("Marjorie Conforme","Manta - Ecuador", R.drawable.imagen_mujer,"$ 12"));
        listaModeladores.add(new Modeladores("Jean Rodriguez","Quito - Ecuador", R.drawable.imagen_hombre,"$ 10"));
        listaModeladores.add(new Modeladores("Maria Choez","Cuenca - Ecuador", R.drawable.imagen_mujer,"$ 10"));
        listaModeladores.add(new Modeladores("Jayden Rodriguez","Manta - Ecuador", R.drawable.imagen_hombre,"$ 15"));
        listaModeladores.add(new Modeladores("ALisson Herrera","Guayaquil - Ecuador", R.drawable.imagen_mujer,"$ 13"));
        listaModeladores.add(new Modeladores("Stefy Calderon","Jaramijo - Ecuador", R.drawable.imagen_mujer,"$ 11"));
        listaModeladores.add(new Modeladores("Galo Quiñonez","Portoviejo - Ecuador", R.drawable.imagen_hombre,"$ 10"));
        listaModeladores.add(new Modeladores("Jose Zambrano","Manta - Ecuador", R.drawable.imagen_hombre,"$ 14"));
        listaModeladores.add(new Modeladores("Genesis Pico","Quito - Ecuador", R.drawable.imagen_mujer,"$ 12"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        getMenuInflater().inflate(R.menu.buscador, menu);
        MenuItem menuItem=menu.findItem(R.id.buscador);
        inflater.inflate(R.menu.menu_cerrar_sesion,menu);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;

        switch (item.getItemId()){

            case R.id.close_sesion:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Seguro que quiere Cerrar la Sesión");
                builder.setTitle("Cerar Sesión");
                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                AlertDialog dialog=builder.create();
                dialog.show();
                break;



        }
        return true;

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase();
        ArrayList<Modeladores> newList = new ArrayList<>();
        for (Modeladores modeladores : listaModeladores){
            String name = modeladores.getNombre().toLowerCase();
            String precio = modeladores.getPrecio().toLowerCase();
            String ciudad = modeladores.getCiudad().toLowerCase();


            if (name.contains(newText)| precio.contains(newText)|ciudad.contains(newText))
                newList.add(modeladores);


        }
        adapter.setFilter(newList);
        return true;
    }
}

