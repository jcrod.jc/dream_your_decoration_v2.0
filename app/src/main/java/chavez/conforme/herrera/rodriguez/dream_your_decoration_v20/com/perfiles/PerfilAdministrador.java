package chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.perfiles;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.administrador.ConsultarContratos;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.administrador.EditarCliente;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.administrador.EditarModelador;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.administrador.MostrarClientes;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.administrador.MostrarModeladores;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin.MainActivity;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.R;

public class PerfilAdministrador extends AppCompatActivity {

Button btnActualizarInf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_administrador);

        btnActualizarInf =(Button)findViewById(R.id.actualizarInfAdmin);

        btnActualizarInf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Se actualizo la información", Toast.LENGTH_SHORT).show();
            }
        });



    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Seguro que quiere cerrar la Sesión");
        builder.setTitle("Cerrar Sesión");
        builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        AlertDialog dialog=builder.create();
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cerrar_sesion,menu);
        getMenuInflater().inflate(R.menu.menuadministrador, menu);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.item1:
                intent=new Intent(PerfilAdministrador.this,MostrarClientes.class);
                startActivity(intent);
                break;
            case R.id.item2:
                intent=new Intent(PerfilAdministrador.this,MostrarModeladores.class);
                startActivity(intent);
                break;
            case R.id.item3:
                intent=new Intent(PerfilAdministrador.this,EditarCliente.class);
                startActivity(intent);
                break;
            case R.id.item4:
                intent=new Intent(PerfilAdministrador.this,EditarModelador.class);
                startActivity(intent);
                break;
            case R.id.item5:
                intent=new Intent(PerfilAdministrador.this,ConsultarContratos.class);
                startActivity(intent);
                break;
        }

        switch (item.getItemId()){

            case R.id.close_sesion:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Seguro que quiere Cerrar la Sesión");
                builder.setTitle("Cerar Sesión");
                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                AlertDialog dialog=builder.create();
                dialog.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
