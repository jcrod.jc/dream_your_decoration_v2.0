package chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.R;


public class RegistroModelador extends Fragment {


    Button btnRegistrar;
    EditText nombreModelador, apellidoModelador, edadModelador, correoModelador, contrasenia1Modelador, contrasenia2Modelador,
            cedulaoModelador, direccionModelador, telefonoModelador, especialidadModelador, precioModelador;
    ImageView imgFoto;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_registro_modelador, container, false);


        nombreModelador = (EditText) view.findViewById(R.id.editTextNombresM);
        apellidoModelador = (EditText) view.findViewById(R.id.editTextApellidosM);
        edadModelador = (EditText) view.findViewById(R.id.editTextEdadM);
        correoModelador = (EditText) view.findViewById(R.id.editTextCorreoM);
        contrasenia1Modelador = (EditText) view.findViewById(R.id.editTextContrasenaM);
        contrasenia2Modelador = (EditText) view.findViewById(R.id.editTextRepitaContrasenaM);
        cedulaoModelador = (EditText) view.findViewById(R.id.editTextCedulaM);
        direccionModelador = (EditText) view.findViewById(R.id.editTextDireccionM);
        telefonoModelador = (EditText) view.findViewById(R.id.editTextTelefonoM);
        especialidadModelador = (EditText) view.findViewById(R.id.editTextEspecialidadM);
        precioModelador = (EditText) view.findViewById(R.id.editTextPrecioM);
        btnRegistrar = (Button) view.findViewById(R.id.buttonEnviarRegistroM);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validarRegistro();










            }
        });


        return view;
    }

    private void validarRegistro() {
        nombreModelador.setError(null);
        apellidoModelador.setError(null);
        edadModelador.setError(null);
        correoModelador.setError(null);
        contrasenia1Modelador.setError(null);
        contrasenia2Modelador.setError(null);
        cedulaoModelador.setError(null);
        direccionModelador.setError(null);
        telefonoModelador.setError(null);
        especialidadModelador.setError(null);
        precioModelador.setError(null);

        String nombre = nombreModelador.getText().toString();
        String apellido = apellidoModelador.getText().toString();
        String edad = edadModelador.getText().toString();
        String correo = correoModelador.getText().toString();
        String contrasenia1 = contrasenia1Modelador.getText().toString();
        String contrasenia2 = contrasenia2Modelador.getText().toString();
        String cedula = cedulaoModelador.getText().toString();
        String direccion = direccionModelador.getText().toString();
        String telefono = telefonoModelador.getText().toString();
        String especislidsd = especialidadModelador.getText().toString();
        String precio = precioModelador.getText().toString();


        if (TextUtils.isEmpty(nombre)) {
            nombreModelador.setError(getString(R.string.campo_obligatorio));
            nombreModelador.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(apellido)) {
            apellidoModelador.setError(getString(R.string.campo_obligatorio));
            apellidoModelador.requestFocus();
        }
        if (TextUtils.isEmpty(edad)) {
            edadModelador.setError(getString(R.string.campo_obligatorio));
            edadModelador.requestFocus();

        }
        if (TextUtils.isEmpty(contrasenia1)) {
            contrasenia1Modelador.setError(getString(R.string.campo_obligatorio));
            contrasenia1Modelador.requestFocus();

        }

        if (TextUtils.isEmpty(contrasenia2)) {
            contrasenia2Modelador.setError(getString(R.string.campo_obligatorio));
            contrasenia2Modelador.requestFocus();

        }
        if (TextUtils.isEmpty(cedula)) {
            cedulaoModelador.setError(getString(R.string.campo_obligatorio));
            cedulaoModelador.requestFocus();

        }
        if (TextUtils.isEmpty(direccion)) {
            direccionModelador.setError(getString(R.string.campo_obligatorio));
            direccionModelador.requestFocus();

        }
        if (TextUtils.isEmpty(telefono)) {
            telefonoModelador.setError(getString(R.string.campo_obligatorio));
            telefonoModelador.requestFocus();

        }
        if (TextUtils.isEmpty(especislidsd)) {
            especialidadModelador.setError(getString(R.string.campo_obligatorio));
            especialidadModelador.requestFocus();

        }
        if (TextUtils.isEmpty(precio)) {
            precioModelador.setError(getString(R.string.campo_obligatorio));
            precioModelador.requestFocus();
            String validemail = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +

                    "\\@" +

                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +

                    "(" +

                    "\\." +

                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +

                    ")+";


            String email = correoModelador.getText().toString();

            Matcher matcher = Pattern.compile(validemail).matcher(email);
            if (TextUtils.isEmpty(correo)) {

                correoModelador.setError(getString(R.string.campo_obligatorio));
                correoModelador.requestFocus();


            }
            if (matcher.matches()) {

            } else {

                correoModelador.setError("Ingrese un correo valido");
                return;
            }



        }

        //Coincidencia de contraseña
        if (contrasenia1.equals(contrasenia2)){


        }else{
            contrasenia2Modelador.setError(getString(R.string.contraseniaNoCoincide));
            contrasenia2Modelador.requestFocus();
            return;
        }
        Toast.makeText(getActivity(), "¡Su registro será confirmado  en  24 horas laborables!", Toast.LENGTH_SHORT).show();

    }
}

