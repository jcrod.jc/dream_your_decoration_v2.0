package chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.R;


public class Registro extends Fragment {

    Button registrarCliente;
    EditText nombreCliente;
    EditText apellidoCliente;
    EditText cedulaCliente;
    EditText correoCliente;
    EditText contraseniaCliente1;
    EditText contraseniaCliente2;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_registro, container, false);
        // Inflate the layout for this fragment
        nombreCliente = (EditText)view.findViewById(R.id.editTextNombresC);
        apellidoCliente = (EditText)view.findViewById(R.id.editTextApellidosC);
        cedulaCliente = (EditText)view.findViewById(R.id.editTextCedulaC);
        correoCliente = (EditText)view.findViewById(R.id.editTextCorreoC);
        contraseniaCliente1 = (EditText)view.findViewById(R.id.editTextContraseniaC1);
        contraseniaCliente2 = (EditText)view.findViewById(R.id.editTextRContraseniaC2);
        registrarCliente =(Button)view.findViewById(R.id.buttonIS);




        registrarCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validar();



            }
        });

        return  view;


}

    private void validar() {

        nombreCliente.setError(null);
        apellidoCliente.setError(null);
        cedulaCliente.setError(null);
        correoCliente.setError(null);
        contraseniaCliente1.setError(null);
        contraseniaCliente2.setError(null);

        String nombre = nombreCliente.getText().toString();
        String apellido = apellidoCliente.getText().toString();
        String cedula = cedulaCliente.getText().toString();
        String correo = correoCliente.getText().toString();
        String contrasenia1 = contraseniaCliente1.getText().toString();
        String contrasenia2 = contraseniaCliente2.getText().toString();

        if (TextUtils.isEmpty(nombre)){
            nombreCliente.setError(getString(R.string.campo_obligatorio));
            nombreCliente.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(apellido)){
            apellidoCliente.setError(getString(R.string.campo_obligatorio));
            apellidoCliente.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(cedula)){
            cedulaCliente.setError(getString(R.string.campo_obligatorio));
            cedulaCliente.requestFocus();
            return;
        }


        if (TextUtils.isEmpty(contrasenia1)){
            contraseniaCliente1.setError(getString(R.string.campo_obligatorio));
            contraseniaCliente1.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(contrasenia2)){
            contraseniaCliente2.setError(getString(R.string.campo_obligatorio));
            contraseniaCliente2.requestFocus();
            return;
        }
        String validemail = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +

                "\\@" +

                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +

                "(" +

                "\\." +

                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +

                ")+";


        String email = correoCliente.getText().toString();

        Matcher matcher= Pattern.compile(validemail).matcher(email);
        if (TextUtils.isEmpty(correo)) {

            correoCliente.setError(getString(R.string.campo_obligatorio));
            correoCliente.requestFocus();


        }if(matcher.matches()){

        } else{

            correoCliente.setError("Ingrese un correo valido");
            return;
        }


        if (contrasenia1.equals(contrasenia2)){


        }else{
            contraseniaCliente2.setError(getString(R.string.contraseniaNoCoincide));
            contraseniaCliente2.requestFocus();
            return;
        }
        Toast.makeText(getActivity(),"¡Se ha registrado con éxito!",Toast.LENGTH_SHORT).show();
    }


}

