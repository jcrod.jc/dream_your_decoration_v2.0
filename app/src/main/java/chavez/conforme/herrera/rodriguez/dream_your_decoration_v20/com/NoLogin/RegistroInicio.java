package chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.MenuItem;

import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.R;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.ui.main.SectionsPagerAdapter;

public class RegistroInicio extends AppCompatActivity {
    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_inicio);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);




        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                if (menuItem.getItemId() == R.id.inicioItem) {

                    Intent intent = new Intent(RegistroInicio.this, MainActivity.class);
                    startActivity(intent);
                }if(menuItem.getItemId() == R.id.LoginItem){
                    Intent intent = new Intent(RegistroInicio.this, InicioSesion.class);
                    startActivity(intent);
                }
                return false;

            }
        });

    }


}