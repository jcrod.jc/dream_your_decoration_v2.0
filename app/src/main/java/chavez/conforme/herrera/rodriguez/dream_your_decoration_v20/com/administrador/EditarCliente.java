package chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.administrador;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.R;

public class EditarCliente extends AppCompatActivity {

    EditText campoNombre,campoApellido,campoCedula,campoCorreo,campoContrasnia;
    Button actualizarCliente,eliminarCliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_cliente);
        setUpActionBar();
        //Pasamos los valores que tendran los EditText en variables
        campoNombre = (EditText)findViewById(R.id.busClienteNombre);
        campoApellido = (EditText)findViewById(R.id.busClienteApellido);
        campoCedula = (EditText)findViewById(R.id.busClienteCedula);
        campoCorreo = (EditText)findViewById(R.id.busClienteCorreo);
        campoContrasnia = (EditText)findViewById(R.id.busClienteContrasenia);

        //Pasamos los valores que tendran los Button en variables
        actualizarCliente = (Button)findViewById(R.id.btnActualizarCliente);
        eliminarCliente = (Button)findViewById(R.id.btnEliminarCliente);


    }

    private void setUpActionBar() {
        ActionBar actionBar=getSupportActionBar();
        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);

        }
    }
}
