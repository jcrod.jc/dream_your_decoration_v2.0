package chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com;

public class Modeladores {

    private String nombre;
    private  String ciudad;
    private int foto;
    private String precio;


    public Modeladores(){

    }

    public Modeladores(String nombre, String ciudad, int foto, String precio) {
        this.nombre = nombre;
        this.ciudad = ciudad;
        this.foto = foto;
        this.precio=precio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }
    public String getPrecio() {
        return precio;
    }
    public void setPrecio(String precio) {
        this.precio = precio;
    }
}
