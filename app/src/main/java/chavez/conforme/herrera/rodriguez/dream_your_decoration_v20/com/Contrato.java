package chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;

public class Contrato extends AppCompatActivity implements View.OnClickListener {

    TextView textFecha,textHora,tipoEspecialidad,textHora2;
    Spinner comboModeladores;
    Button btnFecha,btnHora,btnHora2;
    private int dia,mes,anio,hora,minutos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contrato);

        textFecha =(TextView)findViewById(R.id.txtSelectDia);
        textHora =(TextView)findViewById(R.id.txtSelectHora);
        textHora2 =(TextView)findViewById(R.id.txtSelectHora2);
        tipoEspecialidad =(TextView)findViewById(R.id.IdEspecialidadSeleccionada);

        btnFecha =(Button) findViewById(R.id.selectDia);
        btnHora = (Button)findViewById(R.id.btnSelectHora);
        btnHora2 = (Button)findViewById(R.id.btnSelectHora2);
        comboModeladores = (Spinner)findViewById(R.id.IDTipoModelador);

        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(this,
                R.array.comboModeladores,android.R.layout.simple_spinner_item);
        comboModeladores.setAdapter(adapter);

        comboModeladores.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tipoEspecialidad.setText(adapterView.getItemAtPosition(i).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btnFecha.setOnClickListener(this);
        btnHora.setOnClickListener(this);
        btnHora2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btnFecha){
            final Calendar c= Calendar.getInstance();
            dia = c.get(Calendar.DAY_OF_MONTH);
            mes = c.get(Calendar.MONTH);
            anio = c.get(Calendar.YEAR);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    textFecha.setText(i+"/"+(i1+1)+"/"+i2);
                }
            }
                    ,anio,mes,dia);
            datePickerDialog.show();

        }
        if (v == btnHora){
            final Calendar c= Calendar.getInstance();
            hora = c.get(Calendar.HOUR_OF_DAY);
            minutos = c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int i, int i1) {
                    textHora.setText(i+":"+i1);
                }
            },hora,minutos,false);
            timePickerDialog.show();
        }
        if (v == btnHora2){
            final Calendar c= Calendar.getInstance();
            hora = c.get(Calendar.HOUR_OF_DAY);
            minutos = c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int i, int i1) {
                    textHora2.setText(i+":"+i1);
                }
            },hora,minutos,false);
            timePickerDialog.show();
        }
    }
}
