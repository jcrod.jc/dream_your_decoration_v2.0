package chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.perfiles;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.ContratacionesDeModelador;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin.MainActivity;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.NoLogin.Perfil_Modelador_NoLogin;
import chavez.conforme.herrera.rodriguez.dream_your_decoration_v20.com.R;
import de.hdodenhof.circleimageview.CircleImageView;


public class PerfilModelador extends AppCompatActivity {
    CircleImageView imagen;

    Button btnContrataciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_modelador);
        btnContrataciones = (Button) findViewById(R.id.btnRevisarContratacion);
        imagen = (CircleImageView) findViewById(R.id.picid);

        btnContrataciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PerfilModelador.this, ContratacionesDeModelador.class);
                startActivity(intent);

            }
        });


    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Seguro que quiere cerrar la Sesión");
        builder.setTitle("Cerrar Sesión");
        builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        AlertDialog dialog=builder.create();
        dialog.show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cerrar_sesion, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;

        switch (item.getItemId()) {

            case R.id.close_sesion:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Seguro que quiere Cerrar la Sesión");
                builder.setTitle("Cerar Sesión");
                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                AlertDialog dialog=builder.create();
                dialog.show();
                break;


        }
        return true;
    }

    public void onclick(View view) {

        cargarImagen();
    }

    private void cargarImagen() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        startActivityForResult(Intent.createChooser(intent, "Selecciona una aplicacion"), 10);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri path = data.getData();
            imagen.setImageURI(path);
        }
    }
}
